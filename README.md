# Access notes

The BioMedIT Work Instructions, SOPs, and other documents are available on this repository's WIKI: [BioMedIT Documentation WIKI](https://git.dcc.sib.swiss/dcc/documentation-biomedit/-/wikis/home).

Access is restricted to internal and BioMedIT users.

To request access, click [here](https://git.dcc.sib.swiss/groups/biomedit-documentation/-/group_members/request_access).

If you need assistance or have questions, please contact us at [biomedit@sib.swiss](mailto:biomedit@sib.swiss).

Thank you!

The BioMedIT Central Team